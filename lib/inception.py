from keras import layers as KL
from keras import models as KM
from keras.applications.inception_v3 import InceptionV3
import os

def build_model(class_labels, weights=None, top=False, train_core=False, extract_rep=False):
    val = None
    if weights is not None and os.path.exists(weights):
        val = None
    elif weights == "imagenet":
        val = "imagenet"
    else:
        val = None

    # see https://github.com/keras-team/keras/issues/7085#issuecomment-357757953
    print('weights: {}'.format(val))
    inception_core = InceptionV3(weights=val, include_top=top)
    if train_core is False:
        print('Blocking core layers...')
        for layer in inception_core.layers:
            layer.trainable = False
            if isinstance(layer, KL.normalization.BatchNormalization):
                layer._per_input_updates = {}

    if top == False:
        avg_pool = KL.GlobalAveragePooling2D(name="avg_pool")(inception_core.output)
        # x = KL.Dropout(0.5, name="top_dropout")(x)
        out = KL.Dense(len(class_labels), activation="softmax")(avg_pool)
    else:
        avg_pool = inception_core.get_layer('avg_pool').output
        out = inception_core.output
    model_out = out if not extract_rep else [avg_pool, out]
    model = KM.Model(inputs=[inception_core.input], outputs=model_out)

    if weights is not None and weights != "imagenet":
        print("loading weights...", end="")
        model.load_weights(weights, by_name=True)
        print("done")

    return model


# Preprocess image: center on 0 (gray level is 127) and divide by 128 in order to stay in range [-1,+1]
######################
def custom_process_input(image):
    image -= 127
    image /= 128
    return image
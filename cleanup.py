import numpy as np
import pandas as pd
import os
import errno
from lib import cocoutils
import argparse

CSV_PATH = 'tests'
CSV_PREF = 'aug_'
SET_PREFIXES = ['train', 'valid']
TABLE_SHAPE = (3, 80)
CLASSES = cocoutils.categories()[1:]
ACTION = "none"

ORIG_PATH = "/tmp/data/oid_segmented"
DEST_PATH = "/tmp/data/oid_discarded"


# try/catch utility to avoid exceptions while trying to create a directory
# if it already exists
def try_make_dir(directory):
    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


# checks the dir structure for the subsets
# the structure must be:
# - train
# - - person
# - - airplane
# ...
# - valid
# ...
# - test
# ...
def check_dirs(path, category_dirs):
    try_make_dir(path)
    for dir in category_dirs:
        p = "{}/{}".format(path, dir)
        try_make_dir(p)


def main():
    parser = argparse.ArgumentParser(
        description='Dataset cleanup utility that moves files whose prediction differs fro mthe actual label.')
    parser.add_argument('-a', '--action', help='COCO dataset directory, default: {}'.format(ACTION), type=str, default=ACTION)
    args = parser.parse_args()

    print("checking files...", end="")
    # check csv location
    for set in SET_PREFIXES:
        path = "{}/{}_compare.csv".format(CSV_PATH, set)
        if not os.path.exists(path) and os.path.isfile(path):
            raise NotADirectoryError("Cannot find {}".format(path))
    print("done")

    # count mismatches in every set, for every class, 3 x 80
    mismatch = np.zeros(TABLE_SHAPE).astype(np.int)
    distribs = np.zeros(TABLE_SHAPE).astype(np.int)
    mismatch_files = []

    for index, set in enumerate(SET_PREFIXES):
        print("Checking {} set...".format(set))

        data = pd.read_csv("{}/{}{}_compare.csv".format(CSV_PATH, CSV_PREF, set))

        filenames = data['file']
        y_true = data['label']
        y_pred = data['predictions']
        temp = []

        for i in range(0, len(y_true)):
            cls = y_true[i]
            distribs[index, cls] += 1
            if cls != y_pred[i]:
                mismatch[index, cls] += 1
                temp.append(filenames[i])

        mismatch_files.append(temp)

    comparison = (mismatch / (distribs + 1e-6)) * 100
    np.set_printoptions(formatter={'float': lambda x: "{0:0.2f}".format(x)})

    print("Mismatching labels (%)\n")
    for i, set in enumerate(SET_PREFIXES):
        print("{} SET".format(set.capitalize()))
        print("-" * 20)
        for j in range(0, TABLE_SHAPE[1]):
            print("{:<20s} {:<30s} {:.2f}".format(CLASSES[j], "{:<5s} out of {:d} -> {:d}"
                                                  .format(str(mismatch[i, j]), distribs[i, j],
                                                          distribs[i, j] - mismatch[i, j]), comparison[i, j]))
        print("\n")

    # delete or move files
    if args.action == "move":
        print("Moving files to {}".format(DEST_PATH))
        for set, files in zip(SET_PREFIXES, mismatch_files):
            old_subset_path = "{}/{}".format(ORIG_PATH, set)
            new_subset_path = "{}/{}".format(DEST_PATH, set)
            check_dirs(new_subset_path, cocoutils.catdirs()[1:])

            print("Moving {:d} files from {} set...".format(len(files), set))
            print("Old location: {} \nNew Location: {}".format(old_subset_path, new_subset_path))
            for f in files:
                old_path = "{}/{}".format(old_subset_path, f)
                new_path = "{}/{}".format(new_subset_path, f)
                os.rename(old_path, new_path)
    else:
        print("No action taken. Done!")


if __name__ == '__main__': main()

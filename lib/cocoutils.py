import os
import sys
from collections import Counter


def categories():  # 80 classes + background
    return ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
                   'bus', 'train', 'truck', 'boat', 'traffic light',
                   'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
                   'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
                   'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
                   'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
                   'kite', 'baseball bat', 'baseball glove', 'skateboard',
                   'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
                   'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
                   'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
                   'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
                   'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
                   'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
                   'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
                   'teddy bear', 'hair drier', 'toothbrush']


def catdirs():
    dirs = [c.replace(' ', '_') for c in categories()]
    return dirs


def get_distributions(base_path):
    distributions = dict()
    cat_dirs = catdirs()
    cat_dirs = cat_dirs[1:] #remove BG
    for cat_dir in cat_dirs:
        path = '{}/{}'.format(base_path, cat_dir)
        if not os.path.isdir(path):
            raise NotADirectoryError

        count = len([name for name in os.listdir(path) if os.path.isfile(os.path.join(path, name))])
        distributions[cat_dir] = count

    return distributions


def class_weights(base_path, smoothing=0.1):
    dist = get_distributions(base_path)
    p = max(dist.values()) * smoothing
    for k in dist.keys():
        dist[k] += p
    majority = max(dist.values())
    return {cls: float(majority / count) for cls, count in dist.items()}


def class_weights_generator(generator, smoothing=0.2):
    counter = Counter(generator.classes)
    if smoothing > 0:
        p = max(counter.values()) * smoothing
        for k in counter.keys():
            counter[k] += p
    majority = max(counter.values())
    return {cls: round(float(majority / count), 2) for cls, count in counter.items()}

# Utility function that shows a simple progress bar
def progressbar(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()

def class_distributions():
    return dict([('hair drier', 83),
                 ('toaster', 119),
                 ('sports ball', 471),
                 ('toothbrush', 510),
                 ('baseball glove', 547),
                 ('baseball bat', 573),
                 ('mouse', 579),
                 ('parking meter', 590),
                 ('frisbee', 770),
                 ('scissors', 783),
                 ('remote', 1010),
                 ('snowboard', 1039),
                 ('bear', 1203),
                 ('stop sign', 1212),
                 ('spoon', 1222),
                 ('skis', 1265),
                 ('fire hydrant', 1269),
                 ('microwave', 1319),
                 ('fork', 1525),
                 ('kite', 1584),
                 ('traffic light', 1710),
                 ('knife', 1729),
                 ('hot dog', 1747),
                 ('cell phone', 1789),
                 ('skateboard', 2046),
                 ('keyboard', 2051),
                 ('tennis racket', 2189),
                 ('tie', 2194),
                 ('clock', 2399),
                 ('refrigerator', 2537),
                 ('apple', 2538),
                 ('wine glass', 2754),
                 ('carrot', 2769),
                 ('surfboard', 2817),
                 ('bird', 2862),
                 ('backpack', 2990),
                 ('orange', 2997),
                 ('oven', 3033),
                 ('vase', 3064),
                 ('bicycle', 3105),
                 ('handbag', 3407),
                 ('sandwich', 3437),
                 ('airplane', 3515),
                 ('sink', 3536),
                 ('teddy bear', 3715),
                 ('toilet', 3782),
                 ('sheep', 3860),
                 ('cow', 3863),
                 ('laptop', 3888),
                 ('donut', 3960),
                 ('boat', 3986),
                 ('zebra', 4010),
                 ('suitcase', 4068),
                 ('bed', 4106),
                 ('book', 4126),
                 ('elephant', 4238),
                 ('train', 4289),
                 ('giraffe', 4374),
                 ('horse', 4409),
                 ('dog', 4421),
                 ('cake', 4454),
                 ('cat', 4464),
                 ('banana', 4504),
                 ('potted plant', 4524),
                 ('pizza', 4595),
                 ('bench', 4789),
                 ('bus', 4815),
                 ('broccoli', 4899),
                 ('tv', 4904),
                 ('umbrella', 5188),
                 ('motorcycle', 5348),
                 ('couch', 5565),
                 ('bottle', 5674),
                 ('truck', 5926),
                 ('cup', 7688),
                 ('bowl', 7813),
                 ('car', 11464),
                 ('dining table', 12610),
                 ('chair', 17112),
                 ('person', 122770)])

from __future__ import print_function
from lib import cocoutils
import os
import cv2
import numpy as np

def main():
    TRAIN_PATH = '/tmp/coco/segmentedV2/train'
    TARGET_SIZE = (224, 224)

    print('Counting stats on set: {}'.format(TRAIN_PATH))

    if not os.path.isdir(TRAIN_PATH):
        print('Dataset path is not a valid directory!')
        return -1

    print('reading categories...', end=' ')
    cat_dirs = cocoutils.catdirs()
    paths = ['{}/{}'.format(TRAIN_PATH, c) for c in cat_dirs]
    print('done')

    total = np.zeros((1,3)).astype(np.float)
    count = 0.0

    print('calculating mean...', end=' ')
    for i, path in enumerate(paths):
        for file in os.listdir(path):
            img = cv2.imread('{}/{}'.format(path,file))
            if img is not None:
                total += np.sum(img, axis=(0,1))
                count += 1

    print('done')
    h, w = TARGET_SIZE
    mean = total / (count * h * w)

    print('-'*20)
    print('{:<20s} {}'.format('file count: ', count))
    print('{:<20s} {}'.format('mean: ', mean))

if __name__ == '__main__':
    main()

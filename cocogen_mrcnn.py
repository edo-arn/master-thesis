from lib import cocoutils
import numpy as np
import cv2
import multiprocessing
import os
import errno
import argparse
import sys
from mrcnn import model as modellib
from mrcnn import config
#os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

class CocoConfig(config.Config):
    NAME = "coco"
    IMAGES_PER_GPU = 2
    GPU_COUNT = 1
    NUM_CLASSES = 1 + 80


# Simple configuration class wrapper around relevant data
class Config(object):
    DATA_PATH = '/tmp/data/OID'
    DATA_DEST = '/tmp/data/oid_mixed'
    SET_PREFIXES = ['train2017', 'val2017', 'test2017', 'annotations']
    KERAS_DIRS = ['train', 'valid', 'test'] #added bbox for extra bounding boxes
    GEN_TYPES  = ['segment', 'bbox']
    GEN_SET_VALS = ['all','test','train']
    GEN_SET = 'all'
    BLUR = True
    AREA_THRESHOLD = 2048
    FREQ_THRESHOLD = 1000
    MAX_SAMPLES = [6000, 1000, 2000]
    DILATION_KSIZE = 15
    GAUSSIAN_KSIZE = 51
    PAD_PERCENT = 40
    TARGET_SIZE = (299,299)
    TEST_PERCENT = 10

    AUGMENTATION = True
    AUG_SET = '/usr/home/studenti/sp160362/data/segmented'
    PREFIX = 'oid'
    
    LOG_PATH =  '/usr/home/studenti/sp160362/project/mask-rcnn'
    WEIGHTS_PATH = '/usr/home/studenti/sp160362/project/mask-rcnn/mask_rcnn_coco.h5'
    BATCH_SIZE = 2
    SCORE_THRESHOLD = 0.88
    NOHUP = 'yes'
    SKIP = ['train', 'test']

    def __init__(self):
        self.dilation_kernel = np.ones((self.DILATION_KSIZE, self.DILATION_KSIZE))
        self.gaussian_kernel_size = (self.GAUSSIAN_KSIZE, self.GAUSSIAN_KSIZE)
        self.MODE = self.GEN_TYPES


##########################
#   Utility functions    #
##########################

# Utility function that shows a simple progress bar
def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()


# Ask a yes/no question via raw_input() and return their answer.
def query_yes_no(question):
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    prompt = " [y/n] "

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


# Simple utility that tries to create a directory
# and proceeds if it already exists.
def mkdir(directory):
    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


# Creates the directory structure ready for Keras ImageDataGenerator
def create_dir_structure(base_path, dir_names, categories):
    paths = ['{}/{}'.format(base_path, d) for d in dir_names]
    for path in paths:
        mkdir(path)
        for cat in categories:
            if cat is None or cat == "BG":
                continue
            cat_dir = cat.replace(' ', '_')
            mkdir('{}/{}'.format(path,cat_dir))


def save_image(image, destination, filename, category):
    cat_dir = category.replace(' ', '_')
    path = '{}/{}'.format(destination, cat_dir)
    if os.path.isdir(path):
        cv2.imwrite('{}/{}'.format(path, filename), image, [cv2.IMWRITE_JPEG_QUALITY, 100])


# split a list into evenly sized chunks
def get_chunks(list, size):
    return [list[i:i+size] for i in range(0, len(list), size)]


# gets the cpu count
def get_cpu_count(exclude=2):
    count = multiprocessing.cpu_count()
    return max(1, count - exclude)


def cut_image_keep_aspect(img, bbox, pad=20):
    y1, x1, y2, x2 = bbox
    box_w = np.abs(x1 - x2).astype(int)
    box_h = np.abs(y1 - y2).astype(int)
    img_w, img_h = img.shape[0:2]
    padding = np.round(max(box_w, box_h) * pad / float(100)).astype(int)

    # expand borders based on padding
    box_x_min = int(max(0, x1 - padding))
    box_y_min = int(max(0, y1 - padding))
    box_x_max = int(np.ceil(min(img_h, x2 + padding)))
    box_y_max = int(np.ceil(min(img_w, y2 + padding)))

    # calc aspect ratio, height and width
    new_box_h = np.abs(box_x_max - box_x_min).astype(int)
    new_box_w = np.abs(box_y_max - box_y_min).astype(int)
    aspect = new_box_w / float(new_box_h)

    # square the bbox to max extents
    pad_t = pad_b = pad_l = pad_r = 0
    if aspect > 1:
        pad_l = pad_r = np.ceil((new_box_w - new_box_h) / 2).astype(int)
    elif aspect < 1:
        pad_t = pad_b = np.ceil((new_box_h - new_box_w) / 2).astype(int)

    xm = int(max(0, box_x_min - pad_l))
    ym = int(max(0, box_y_min - pad_b))
    xM = int(min(img_h, box_x_max + pad_r))
    yM = int(min(img_w, box_y_max + pad_t))
    return img[ym:yM,xm:xM,:]


# Cuts the image and the given mask using the given coordinates and extensions
def cut_image_and_mask(img, mask, bbox, pad=32):
    y1, x1, y2, x2 = bbox
    shape = img.shape[0:2]
    xm = int(max(0, x1 - pad))
    ym = int(max(0, y1 - pad))
    xM = int(np.ceil(min(shape[1], x2 + pad)))
    yM = int(np.ceil(min(shape[0], y2 + pad)))
    return img[ym:yM,xm:xM,:], mask[ym:yM,xm:xM]


# converts the image to normalized float values [0,1]
def im2double(image,maxval=255.0):
    return image / maxval


# converts the image back to 256 levels per channel
def im2uint8(image):
    return (image * 255).astype(np.uint8)


# Rescales the given image and mask to fit the given size tuple (square by default)
# Credits to: https://stackoverflow.com/questions/44720580/resize-image-canvas-to-maintain-square-aspect-ratio-in-python-opencv
def resize_and_pad(img, mask, size, padColor):
    h, w = img.shape[:2]
    sh, sw = size
    # interpolation method
    if h > sh or w > sw:  # shrinking image
        interp = cv2.INTER_AREA
    else:  # stretching image
        interp = cv2.INTER_LINEAR
    # aspect ratio
    aspect = float(w) / h

    if aspect > 1:  # horizontal
        new_w = sw
        new_h = np.round(new_w / aspect).astype(int)
        pad_vert = (sh - new_h) / 2
        pad_top, pad_bot = np.floor(pad_vert).astype(int), np.ceil(pad_vert).astype(int)
        pad_left, pad_right = 0, 0
    elif aspect < 1:  # vertical
        new_h = sh
        new_w = np.round(new_h * aspect).astype(int)
        pad_horz = (sw - new_w) / 2
        pad_left, pad_right = np.floor(pad_horz).astype(int), np.ceil(pad_horz).astype(int)
        pad_top, pad_bot = 0, 0
    else:  # square
        new_h, new_w = sh, sw
        pad_left, pad_right, pad_top, pad_bot = 0, 0, 0, 0
    # scale and pad
    scaled_img = cv2.resize(img, (new_w, new_h), interpolation=interp)
    scaled_img = cv2.copyMakeBorder(scaled_img, pad_top, pad_bot, pad_left, pad_right, borderType=cv2.BORDER_CONSTANT, value=padColor)
    scaled_msk = cv2.resize(mask, (new_w, new_h), interpolation=interp)
    scaled_msk = cv2.copyMakeBorder(scaled_msk, pad_top, pad_bot, pad_left, pad_right, borderType=cv2.BORDER_CONSTANT, value=padColor)

    return scaled_img, scaled_msk


# Get the area of a RoI bounding box, format [y1, x1, y2, x2]
def calc_area(box):
    y1, x1, y2, x2 = box
    h = abs(y2 - y1)
    b = abs(x2 - x1)
    return h * b


##########################
#         Main           #
##########################
def main():
    print("Initializing generator...")
    config = Config()
    np.random.seed(42)

    model_config = CocoConfig()
    model = modellib.MaskRCNN(mode="inference", model_dir=config.LOG_PATH, config=model_config)

    parser = argparse.ArgumentParser(description='COCO dataset utility that generates a processed set of masked images.')
    parser.add_argument('-p', '--path', help='COCO dataset directory, default: {}'.format(config.DATA_PATH), nargs='?',
                        const=config.DATA_PATH, type=str, default=config.DATA_PATH)
    parser.add_argument('-d', '--dest', help='Target directory, default: {}'.format(config.DATA_DEST), nargs='?',
                        const=config.DATA_DEST, type=str, default=config.DATA_DEST)
    parser.add_argument('-s', '--set', help='Which sets should be generated, "test" or "all" (default)', nargs='?',
                        const=config.GEN_SET, type=str, default=config.GEN_SET)
    parser.add_argument('-n', '--nohup', help='Do not ask for confirmation and execute immediately', nargs='?',
                        const=config.NOHUP, type=str, default=config.NOHUP)

    args = vars(parser.parse_args())

    # Basic setup
    DATA_PATH = args['path']
    DATA_DEST = args['dest']
    GEN_SET = args['set']
    NOHUP = args['nohup']

    # print config and ask for confirmation
    print('-'*20)
    print('{:<20s} {:<80s}'.format('COCO directory:', DATA_PATH))
    print('{:<20s} {:<80s}'.format('Target folder:', DATA_DEST))
    print('{:<20s} {:<80s}'.format('Sets to process:', GEN_SET))
    print('{:<20s} {:<80s}'.format('Target img size:', str(config.TARGET_SIZE)))
    par = 'blur: {} - dil: {} px - gauss:{} px - max area: {} px'\
        .format(config.BLUR, config.DILATION_KSIZE, config.GAUSSIAN_KSIZE, config.AREA_THRESHOLD)
    print('{:<20s} {:<80s}'.format('Parameters:', par))
    print('{:<20s} {:<80s}'.format('Test set percent.:', str(config.TEST_PERCENT)))
    print('-'*20)

    if NOHUP == 'no':
        if not query_yes_no('Continue?'):
            print('Canceled.')
            return

    if GEN_SET not in config.GEN_SET_VALS:
        raise ValueError('The specified set is not valid!')

    DATA_SUBDIRS = ['{}/{}'.format(DATA_PATH, pref) for pref in config.SET_PREFIXES]

    # Check whether the given path exists and has the correct structure
    if not os.path.isdir(DATA_PATH):
        raise FileNotFoundError('The specified COCO path cannot be found.')
    else:
        for dir in DATA_SUBDIRS:
            if (not os.path.isdir(dir)):
                err = "{} is not a valid directory.".format(dir)
                raise ValueError(err)
        print('COCO dataset found at {}.'.format(DATA_PATH))

    frequencies = cocoutils.class_distributions()

    # Create the folder structure for Keras in the target directory
    print("Checking directory structure...", end="")
    create_dir_structure(DATA_DEST, config.KERAS_DIRS, cocoutils.categories())
    print("done")

    print("Loading model weights...", end="")
    model.load_weights(config.WEIGHTS_PATH, by_name=True)
    print("done")

    # Iterate over COCO folders (positions 0,1,2, exluding 3 since it contains annotations dir
    for i, coco_set in enumerate(DATA_SUBDIRS[0:-1]):

        if config.KERAS_DIRS[i] in config.SKIP:
            continue
            
        print("Processing {}...".format(coco_set))
        dest_path = '{}/{}'.format(DATA_DEST, config.KERAS_DIRS[i])
        max_samples = config.MAX_SAMPLES[i]

        # load image filenames
        images = os.listdir(coco_set)
        total = len(images)
        print("Total images in {}: {}".format(config.SET_PREFIXES[i], total))

        # Iterate the image IDs, get the image and the annotations and produce the clipped pictures
        done = 0
        categories = cocoutils.categories()
        categoryCount = np.zeros(len(categories)).astype(np.int)
        tooSmallCount = np.zeros(len(categories)).astype(np.int)
        lowScoreCount = np.zeros(len(categories)).astype(np.int)

        # counting already present data
        required_amounts = dict()
        if config.AUGMENTATION:
            print("Augmenting set: getting current distributions...", end="")
            current_amounts = cocoutils.get_distributions("{}/{}".format(config.AUG_SET, config.KERAS_DIRS[i])) #uses catdirs -> replace spaces with _
            for cat in categories:
                required_amounts[cat] = max(0, max_samples - current_amounts.get(cat.replace(" ", "_"), max_samples))
            print("done")
        else:
            print("Creating new set: setting distributions to max samples.")
            vals = [(cat, config.MAX_SAMPLES[i]) for cat in categories]
            required_amounts = dict(vals)

        # Iterate over the images in batches
        progress(done, total, suffix='{}/{}'.format(done, total))
        for b in range(0, len(images), config.BATCH_SIZE):
            files = images[b:b+config.BATCH_SIZE]
            if len(files) < config.BATCH_SIZE:
                print("last batch is too small, discarded.")
                continue

            #read images GBR and convert to RGB (not strictly necessary, but you never know)
            batch = [cv2.imread(os.path.join(coco_set, f), cv2.IMREAD_COLOR) for f in files]
            batch = [img[:,:,::-1] for img in batch]
            # run detect, that it already converts to the correct format
            result = model.detect(batch, verbose=0)

            # iterate over batch results
            for batch_index, r in enumerate(result):
                image = batch[batch_index]

                masks = r['masks']
                masks = np.transpose(masks, (2, 0, 1)) #order [N, height, width]
                scores = r['scores']
                class_ids = r['class_ids']
                boxes = r['rois']

                # iterate over RoIs of a single image in the batch
                for j in range(len(scores)):
                    id = class_ids[j]
                    area = calc_area(boxes[j])
                    mask = masks[j].astype(np.uint8)
                    cat  = categories[id]

                    if categoryCount[id] >= required_amounts[cat]:
                        lowScoreCount[id] += 1
                        continue
                    if scores[j] < config.SCORE_THRESHOLD:
                        lowScoreCount[id] += 1
                        continue
                    elif area <= config.AREA_THRESHOLD and frequencies[categories[id]] >= config.FREQ_THRESHOLD:
                        tooSmallCount[id] += 1
                        continue

                    #process image
                    if 'segment' in config.MODE:
                        img_cut, mask = cut_image_and_mask(im2double(image), mask, boxes[j])
                        img_scaled, mask = resize_and_pad(img_cut, mask, size=config.TARGET_SIZE, padColor=[0.5, 0.5, 0.5])
                        if config.BLUR:
                            mask = cv2.dilate(mask, config.dilation_kernel, iterations=1)
                            mask = np.clip(cv2.GaussianBlur(mask.astype(np.float), config.gaussian_kernel_size, 0,
                                                            borderType=cv2.BORDER_CONSTANT), 0, 1)
                        mask = mask[...,np.newaxis]
                        result =img_scaled * mask
                        background = (1 - mask) * 0.5
                        result += background
                        result = result[:,:,::-1] #store result (revert to BGR for imsave)
                        name = '{}-{}_{}{}_s.jpg'.format(config.PREFIX, files[batch_index][:-4], batch_index, j)
                        save_image(im2uint8(result), dest_path, name, cat)
                    if 'bbox' in config.MODE:
                        img = cut_image_keep_aspect(image, boxes[j],pad=config.PAD_PERCENT)
                        result = cv2.resize(img, config.TARGET_SIZE, interpolation=cv2.INTER_LINEAR)
                        #result = result[:, :, ::-1] #store result (revert to BGR for imsave)
                        name = '{}-{}_{}{}_b.jpg'.format(config.PREFIX, files[batch_index][:-4], batch_index, j)
                        save_image(result[:,:,::-1], dest_path, name, cat)

                    # update count for each category
                    categoryCount[id] += 1

            # update progress
            done += config.BATCH_SIZE
            cc = np.sum(categoryCount)
            lc = np.sum(lowScoreCount)
            sc = np.sum(tooSmallCount)
            progress(done, total, suffix='{}/{}, gen: {}, ts: {}, ls: {}'.format(done, total, cc, sc, lc))

        # print result
        progress(total, total, suffix='{}/{}'.format(total, total))
        print('Completed path: {}'.format(coco_set))
        print('Generated:')
        print('-'*20)
        for k, count in enumerate(categoryCount):
            if categories[k] is None: continue
            print('{:<20s} {:<10s}'.format(categories[k], str(count)))

    # Done!
    print('Images successfully generated!')


### Execution
if __name__ == '__main__': main()

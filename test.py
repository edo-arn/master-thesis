from __future__ import print_function

from lib import cocoutils, inception
from keras.optimizers import Adam
from keras.preprocessing.image import ImageDataGenerator
import numpy as np
import pandas as pd
import argparse
import os
import json

try:
    from keras.engine import saving
except ImportError: # Keras before 2.2 used the 'topology' namespace.
    from keras.engine import topology as saving

# Uncomment this for cpu-only usage
#import os
#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


# Config data
######################

class TestConfig(object):
    MODEL_PATH = './inception_segm_60ep.h5'
    PATH_TEST = '/usr/home/studenti/sp160362/data/segmented'

    CLASS_NAMES = cocoutils.categories()[1:]
    CLASS_DIRS = cocoutils.catdirs()[1:]
    TARGET_SIZE = (299, 299)
    BATCH_SIZE = 64

    RESULT_PATH = './tests'
    RESULT_NAME = 'predictions'
    ACTION = 'extract'
    ALLOWED_ACTIONS = ['test', 'predict', 'extract', 'extract-parallel']

    def show(self):
        print('{:<20s} {:<80s}'.format('Trained model path:', self.MODEL_PATH))
        print('{:<20s} {:<80s}'.format('Test directory:', str(self.PATH_TEST)))
        print('-'*20)



def read_labels(path):
    with open(path)as file:
        result = json.load(file)
    result = {int(k): v for k, v in result.items()}
    return result


# Main
######################
def main():
    # create config
    cfg = TestConfig()

    # init argparse
    parser = argparse.ArgumentParser(description='COCO dataset utility that generates a processed set of masked images.')
    parser.add_argument('-w', '--weights', help='model weights file path.', nargs='?', const=cfg.MODEL_PATH,
                        type=str, default=cfg.MODEL_PATH)
    parser.add_argument('-s', '--set', help='test set directory', nargs='?', const=cfg.PATH_TEST, type=str,
                        default=cfg.PATH_TEST)
    parser.add_argument('-a', '--action', help='what to do: test, predict or extract.', nargs='?', const=cfg.ACTION,
                        type=str, default=cfg.ACTION)
    parser.add_argument('-r', '--result', help='result name, if anything gets saved.', nargs='?', const=cfg.RESULT_NAME,
                        type=str, default=cfg.RESULT_NAME)
    parser.add_argument('-i', '--indices', help='file containing which indices to save', nargs='?',
                        type=str, default=None)
    parser.add_argument('-l', '--labels', help='file containing which classes to process', nargs='?',
                        type=str, default=None)


    args = vars(parser.parse_args())
    coco_labels = read_labels(os.path.join('labels', 'coco-labels.json'))
    label_to_id = {v: k for k, v in coco_labels.items()}

    WEIGHTS_PATH = args['weights']
    WEIGHTS_PATH = WEIGHTS_PATH if WEIGHTS_PATH != "" else None
    TEST_PATH  = args['set']
    ACTION = args['action']
    RESULT_NAME = args['result']
    IND_FILE = args['indices']

    labels_path = args['labels']
    labels_path = labels_path if labels_path is not None else os.path.join('labels', 'coco-labels.json')

    labels_dict = read_labels(labels_path)
    class_dirs = [l.replace(' ','_') for l in labels_dict.values()]
    id_table = {new_id: label_to_id[name] for new_id, name in labels_dict.items()} #converts [0-10] -> [0-80]
    inverse_id_table = {v: k for k, v in id_table.items()} #converts [0-80] -> [0-10]

    if ACTION not in cfg.ALLOWED_ACTIONS:
        raise ValueError("Action '{}' not recognised. Allowed actions: {}".format(ACTION, cfg.ALLOWED_ACTIONS))

    print('-'*20)
    print('{:<20s} {:<80s}'.format('Model path:', WEIGHTS_PATH))
    print('{:<20s} {:<80s}'.format('Test dir: ' , TEST_PATH))
    print('-'*20)

    print('building model...', end=' ')
    extract = ACTION == 'extract'
    opt = Adam(lr=1e-3)
    model = inception.build_model(cfg.CLASS_NAMES, weights=WEIGHTS_PATH, extract_rep=extract)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    print('done')

    #prepare generator
    generator = ImageDataGenerator(preprocessing_function=inception.custom_process_input)
    test_batches = generator.flow_from_directory(TEST_PATH,
                                                  batch_size = cfg.BATCH_SIZE,
                                                  classes = class_dirs,
                                                  target_size = cfg.TARGET_SIZE, shuffle=False)

    # test model
    filenames = np.array(test_batches.filenames)
    steps = int(np.ceil(len(filenames)/cfg.BATCH_SIZE))

    if ACTION == 'predict':
        print('predicting categories...', end=' ')
        predictions = model.predict_generator(test_batches, steps=steps, verbose=1)
        print('done')
        print('saving results to {} ...'.format(cfg.RESULT_PATH), end=' ')

        #convert to same labels (using same range), use -1 when the label is wrong and is not in the dict
        pred = np.argmax(predictions, axis=-1).astype(int)
        pred = [inverse_id_table.get(p, -1) for p in pred]
        # save results to file
        result = pd.DataFrame({'filenames': filenames, 'predictions': pred, 'labels': test_batches.classes})
        result.to_csv('{}/{}.csv'.format(cfg.RESULT_PATH, RESULT_NAME))
        print('done')

    elif ACTION == 'test':
        print('testing set...')
        result = model.evaluate_generator(test_batches,steps=steps)
        print('loss: {:.5f} - acc: {:.5f}'.format(result[0], result[1]))

    elif ACTION == 'extract':
        print('predicting categories and extracting representations...')
        representations, predictions = model.predict_generator(test_batches, steps=steps, verbose=1)
        print("Rep. shape: ", representations.shape)
        print("Pred. shape:",predictions.shape)

        #convert predictions from full range [0-80] to [0-actual num. of classes]
        true_labels = test_batches.classes
        pred_labels = np.array([inverse_id_table.get(p, -1) for p in np.argmax(predictions, axis=-1)])
        # pack values and order for deterministic results
        result = pd.DataFrame({'filenames': filenames,
                               'predictions': pred_labels,
                               'labels': true_labels,
                               'order': np.arange(0, len(representations))
                               })
        result.sort_values('filenames', inplace=True)

        # select correct ones if no index file is provided
        if IND_FILE is None:
            print('Only extracting correct predictions')
            indices = np.where(true_labels == pred_labels)[0]
        # select representations based on the provided index file otherwise
        else:
            selected = pd.read_csv(IND_FILE, index_col=0)
            selected.sort_values('filenames', inplace=True)
            indices = selected['indices'].values
            #check if files match
            current_files = result['filenames'].values[indices]
            current_files = np.array(['_'.join(s.split('_')[:-1]) for s in current_files]) #remove the s/b suffix
            expected_files = selected['filenames'].values

            print('File selection: {} - {}'.format(len(current_files), expected_files.shape))
            comparison = np.array(current_files == expected_files)
            if not np.all(comparison):
                mismatch = np.where(comparison == False)[0]
                err = pd.DataFrame({'indices': mismatch,
                                    'current': current_files[mismatch],
                                    'expected': expected_files[mismatch]})
                print('File mismatch!')
                print(err)
                raise AssertionError('Files do not match')
            print('Extracting selected indices')

        # check if predicted and actual values pointed by indices match
        print(indices.shape)
        print("{} predictions out of {}".format(indices.size, true_labels.size))
        assert np.all(result['predictions'].values[indices] == result['labels'].values[indices]),\
            'Selected indices are not correct!'

        # prepare a new empty array, sort representations in the same order and extract using indices,
        # then save to file (using npy for better memory usage)
        data = np.empty((indices.size, representations.shape[1] + 1))
        print("Saving representations and labels into a numpy file [shape {}]...".format(data.shape), end="")
        representations = representations[result['order'].values]
        data[:,:-1] = representations[indices]
        data[:,-1]  = result['labels'].values[indices]
        np.save("{}/representations.npy".format(cfg.RESULT_PATH), data)
        print("done.")

    elif ACTION =='extract-parallel':
        print('Extracting representations from bbox and segmentation sets...')
        print('Reading true labels and predictions...', end='')
        #gen from txt keeps the row index and adds a row for column names
        y_pred_b = pd.read_csv(os.path.join(cfg.RESULT_PATH, 'pred_bbox.csv'), index_col=0)
        y_pred_s = pd.read_csv(os.path.join(cfg.RESULT_PATH, 'pred_segm.csv'), index_col=0)
        y_pred_b.sort_values('filenames', inplace=True)
        y_pred_s.sort_values('filenames', inplace=True)
        print('done.')

        assert y_pred_b.shape == y_pred_s.shape, 'Dataset shapes do not match!'
        roots_s = ['_'.join(s.split('_')[:-1]) for s in y_pred_s['filenames'].values]
        roots_b = ['_'.join(b.split('_')[:-1]) for b in y_pred_b['filenames'].values]

        assert np.all(roots_s == roots_b), 'Files not in the correct order!'
        assert np.all(y_pred_b['labels'].values == y_pred_s['labels'].values), 'Labels do not match!'

        y_true = y_pred_b['labels'].values
        y_labels_s = y_pred_s['predictions'].values
        y_labels_b = y_pred_b['predictions'].values

        labels = np.vstack((y_true, y_labels_s, y_labels_b)).transpose()
        equals = np.array([(row == row[0]).all() for row in labels])
        print('labels matrix: {}'.format(labels.shape))

        print('Available {}/{} representations'.format(np.count_nonzero(equals), len(equals)))
        available_indices = equals.nonzero()[0]
        available_files = np.array(roots_s)[available_indices]
        print(available_indices)
        print(available_files)
        result = pd.DataFrame({'indices': available_indices, 'filenames': available_files})

        print('Saving indices to file...', end='')
        result.to_csv('{}/{}.csv'.format(cfg.RESULT_PATH, 'selection'))
        print('done.')
    print('testing done.')

### Execution
if __name__ == '__main__': main()

import os
import errno
from lib import cocoutils
import argparse
import numpy as np


SET_PREFIXES = ['train', 'valid', 'test']
ORIG_PATH = "/tmp/data/mixed"
DEST_PATH_S = "/tmp/data/segmented"
DEST_PATH_B = "/tmp/data/bbox"

DEFAULT_ACTION = 'separate'
ALLOWED_ACTIONS = ['separate', 'subsample']
SAMPLE_SIZE = 4000


# try/catch utility to avoid exceptions while trying to create a directory
# if it already exists
def try_make_dir(directory):
    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


# checks the dir structure for the subsets
def check_dirs(path, category_dirs):
    try_make_dir(path)
    for dir in category_dirs:
        p = "{}/{}".format(path, dir)
        try_make_dir(p)


def separate(origin, segment_path, bbox_path):
    print("separating files...", end="")

    for index, set in enumerate(SET_PREFIXES):
        print("Separating files in {} set...".format(set))
        mixed_subpath = os.path.join(origin, set)
        segm_subpath = os.path.join(segment_path, set)
        bbox_subpath = os.path.join(bbox_path, set)
        check_dirs(segm_subpath, cocoutils.catdirs()[1:])
        check_dirs(bbox_subpath, cocoutils.catdirs()[1:])

        for cat in cocoutils.catdirs()[1:]:
            old_cat_path = os.path.join(mixed_subpath, cat)
            seg_cat_path = os.path.join(segm_subpath, cat)
            box_cat_path = os.path.join(bbox_subpath, cat)
            filenames = [f for f in os.listdir(old_cat_path) if os.path.isfile(os.path.join(old_cat_path, f))]
            size = len(filenames)
            assert size % 2 == 0, 'Category {} does not contain image pairs: {}'.format(cat, size)
            print('Moving category: "{}, size: {}"'.format(cat, size))

            for f in filenames:
                dest = seg_cat_path if '_s' in f else box_cat_path
                os.rename(os.path.join(old_cat_path, f), os.path.join(dest, f))

        print('Done! Checking results...')
        check_parallel(segment_path, bbox_path, [set])
    print('Done, enjoy!')


def subsample_data(path_s, path_b, sets, n_samples=5000, path_bin='/tmp/data/extra'):
    print('Subsampling {} images for each category...'.format(n_samples))
    for set in sets:
        subpath_s = os.path.join(path_s, set)
        subpath_b = os.path.join(path_b, set)
        subpath_d = os.path.join(path_bin, set)
        check_dirs(subpath_d, cocoutils.catdirs()[1:])
        print('Processing set: {}'.format(set))

        for cat in cocoutils.catdirs()[1:]:
            print("Subsampling '{}'...".format(cat), end='')
            cat_path_s = os.path.join(subpath_s, cat)
            cat_path_b = os.path.join(subpath_b, cat)
            cat_path_d = os.path.join(subpath_d, cat)

            files_s = np.array(os.listdir(cat_path_s))
            files_b = np.array(os.listdir(cat_path_b))
            assert len(files_s) == len(files_b), "File count mismatch for {} ({})".format(set, cat)
            len_s = len(files_s)
            if len_s == 0:
                print('Category {}/{} has 0 instances, skipping'.format(set, cat))
                continue
            count = min(len_s, n_samples)

            # select n random indices
            subsampled = np.random.choice(len_s, size=count, replace=False)

            # create a mask that excludes the selection
            mask = np.ones(len_s, dtype=bool)
            mask[subsampled] = False
            remaining_indices = mask.nonzero()[0] #returns a tuple

            # delete the remaining ones (move to bin folder)
            for i in remaining_indices:
                f_s = files_s[i]
                f_b = files_b[i]
                f_s1 = f_s.split('_')
                f_b1 = f_b.split('_')
                #check image id and segmentation index
                assert f_s1[0] == f_b1[0] and f_s1[1] == f_b1[1], 'File mismatch! {} in {}/{}'.format(f_s, set, cat)
                os.rename(os.path.join(cat_path_s, f_s), os.path.join(cat_path_d, f_s))
                os.rename(os.path.join(cat_path_b, f_b), os.path.join(cat_path_d, f_b))
            print('ok!')


def discard_parallel(source_path, dest_path, sets):
    print('Discarding corresponding files from {} in {}...'.format(source_path, dest_path))
    suffix = 's' if 'segmented' in dest_path else 'b'
    for set in sets:
        print('Processing set: {}...'.format(set), end='')
        subset_path_disc = os.path.join(source_path, set)
        subset_path_dest = os.path.join(dest_path, set)
        for cat in cocoutils.catdirs()[1:]:
            cat_path_disc = os.path.join(subset_path_disc, cat)
            cat_path_dest = os.path.join(subset_path_dest, cat)
            files_to_remove = os.listdir(cat_path_disc)
            if len(files_to_remove) == 0:
                print('{}/{} is empty, continuing...'.format(set, cat))

            for f in files_to_remove:
                parts = f.split('_')
                basename = '_'.join(parts[:-1])
                corresponding = '{}_{}.jpg'.format(basename, suffix)
                full_path = os.path.join(cat_path_dest, corresponding)
                if not os.path.exists(full_path):
                   print('Missing file: {}, perhaps already removed?'.format(full_path))
                   continue
                # moving to discarded folder
                os.rename(full_path, os.path.join(cat_path_disc, corresponding))
        print('done!')
    print('Removal completed!')


def check_parallel(segm_path, bbox_path, sets):
    print('Checking {} and {} for symmetry...'.format(segm_path, bbox_path))
    for set in sets:
        print('Checking set: {}'.format(set))
        subset_segm = os.path.join(segm_path, set)
        subset_bbox = os.path.join(bbox_path, set)
        for cat in cocoutils.catdirs()[1:]:
            print('checking \'{}\'...'.format(cat), end='')
            seg_cat_path = os.path.join(subset_segm, cat)
            box_cat_path = os.path.join(subset_bbox, cat)
            filenames_s = os.listdir(seg_cat_path)
            filenames_b = os.listdir(box_cat_path)
            assert len(filenames_s) == len(filenames_b), 'file counts do not correspond for category: {}'.format(cat)
            print('# files: {} <-> {} ...'.format(len(filenames_s), len(filenames_b)))
            basenames_s = ['_'.join(f.split('_')[:-1]) for f in filenames_s]
            basenames_b = ['_'.join(f.split('_')[:-1]) for f in filenames_b]  
            for f in basenames_s:
                if f not in basenames_b:
                    print('File in s. {} not present in b.'.format(f))

            for f in basenames_b:
                if f not in basenames_s:
                    print('File in b. {} not present in s.'.format(f))
                    
    print('Checking done!')

def main():
    parser = argparse.ArgumentParser(description='COCOGen utilities: separate bounding boxes from segmentations, subsampling...')
    parser.add_argument('-n', '--action', help='Which action to perform (separate, sample,...)', type=str, default=DEFAULT_ACTION)
    parser.add_argument('-r', '--source', help='Original dataset', type=str, default=ORIG_PATH)
    parser.add_argument('-db', '--destb', help='Destination path for the bbox set', type=str, default=DEST_PATH_B)
    parser.add_argument('-ds', '--dests', help='Destination path for the segm set', type=str, default=DEST_PATH_S)
    parser.add_argument('-s', '--size', help='Size of the subsampling (used when action = subsample),', type=int, default=SAMPLE_SIZE)
    args = parser.parse_args()

    if args.action == 'separate':
        separate(args.source, args.dests, args.destb)
    elif args.action == 'subsample':
        subsample_data(args.dests, args.destb, SET_PREFIXES, n_samples=args.size)
    elif args.action == 'discard':
        discard_parallel(args.source, args.destb, SET_PREFIXES)
    elif args.action == 'check':
        check_parallel(args.dests, args.destb, SET_PREFIXES)
    else:
        raise ValueError('Action "{}" uknown. Allowed values: {}'.format(args.action, ALLOWED_ACTIONS))

if __name__ == '__main__': main()

from keras.preprocessing import image
import numpy as np
import pandas as pd
from lib import cocoutils, inception


class Config(object):
    MODEL_PATH = '/usr/home/studenti/sp160362/project/models/inception/inception_coco_3.h5'
    DATA_PATH = '/tmp/data/oid_segmented'
    DATA_SETS = ['train', 'valid', 'test']
    DATA_PREFIX = 'aug_'

    CLASS_NAMES = cocoutils.categories()[1:]
    CLASS_DIRS = cocoutils.catdirs()[1:]
    TARGET_SIZE = (299, 299)
    BATCH_SIZE = 64
    MEAN_PIXEL  = [128.0, 128.0, 128.0]
    LEARNING_RATE = 1e-3

    RESULT_PATH = './tests'
    SAVE_RESULT = True


def main():
    print("Dataset cleanup starting!")
    cfg = Config()
    # Load model
    model = inception.build_model(cfg.CLASS_DIRS, weights=cfg.MODEL_PATH)

    for subset in cfg.DATA_SETS:
        print("Processing set: {}".format(subset))
        path = "{}/{}".format(cfg.DATA_PATH, subset)
        generator = image.ImageDataGenerator(preprocessing_function=inception.custom_process_input)
        batches = generator.flow_from_directory(path, cfg.TARGET_SIZE,
                                                classes=cfg.CLASS_DIRS,
                                                batch_size=cfg.BATCH_SIZE,
                                                shuffle=False)

        num_files = len(batches.filenames)
        print(batches.classes)
        steps = int(np.ceil(num_files / float(cfg.BATCH_SIZE)))
        print("# files: {}, batch size: {} -> required generator steps: {}".format(num_files, cfg.BATCH_SIZE, steps))

        # Create dataframe for the current labels
        df = pd.DataFrame({"file": batches.filenames, "label": batches.classes})

        if cfg.SAVE_RESULT:
            print('Predicting classes...')
            result = model.predict_generator(batches, steps=steps, verbose=1)
            predictions = np.argmax(result, axis=-1)

            #Save predictions
            print("Saving results to csv...", end=" ")
            assert len(predictions) == num_files
            df["predictions"] = predictions
            df.to_csv("{}/{}{}_compare.csv".format(cfg.RESULT_PATH, cfg.DATA_PREFIX, subset))
        else:
            model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
            print('Evaluating performances...')
            result = model.evaluate_generator(batches, steps=steps)
            print(result)
        print("Done!")


if __name__ == '__main__': main()

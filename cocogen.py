from pycocotools import coco as cocoapi
from lib import cocoutils
import numpy as np
import cv2
import multiprocessing
import os
import errno
import argparse
import sys

# Simple configuration class wrapper around relevant data
class Config(object):
    COCO_PATH = '/usr/home/studenti/sp160362/data/coco'
    COCO_DEST = '/tmp/data/bbox_train'
    COCO_PREFIXES = ['train2017', 'val2017', 'test2017', 'annotations']
    KERAS_DIRS = ['train', 'valid', 'test'] #added bbox for extra bounding boxes
    GEN_TYPES  = ['segment', 'bbox']
    GEN_SET_VALS = ['all','test']
    GEN_SET = 'all'
    BLUR = True
    AREA_THRESHOLD = 2048
    FREQ_THRESHOLD = 1000
    MAX_SAMPLES = [6000, 1000, 1000]
    DILATION_KSIZE = 19
    GAUSSIAN_KSIZE = 61
    TARGET_SIZE = (224,224)
    TEST_PERCENT = 10
    NOHUP = 'yes'

    def __init__(self):
        self.dilation_kernel = np.ones((self.DILATION_KSIZE, self.DILATION_KSIZE))
        self.gaussian_kernel_size = (self.GAUSSIAN_KSIZE, self.GAUSSIAN_KSIZE)
        self.MODE = self.GEN_TYPES[1]


##########################
#   Utility functions    #
##########################

# Utility function that shows a simple progress bar
def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))
    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)
    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()


# Ask a yes/no question via raw_input() and return their answer.
def query_yes_no(question):
    try:
        input = raw_input
    except NameError:
        pass

    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    prompt = " [y/n] "

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


# Simple utility that tries to create a directory
# and proceeds if it already exists.
def mkdir(directory):
    try:
        os.makedirs(directory)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


# Creates the directory structure ready for Keras ImageDataGenerator
def create_dir_structure(base_path, dir_names, categories):
    paths = ['{}/{}'.format(base_path, d) for d in dir_names]
    for path in paths:
        mkdir(path)
        for cat in categories:
            if cat is None:
                continue
            cat_dir = cat.replace(' ', '_')
            mkdir('{}/{}'.format(path,cat_dir))

def save_image(image, destination, filename, category):
    cat_dir = category.replace(' ', '_')
    path = '{}/{}'.format(destination, cat_dir)
    if os.path.isdir(path):
        cv2.imwrite('{}/{}'.format(path, filename), image)


# split a list into evenly sized chunks
def get_chunks(list, size):
    return [list[i:i+size] for i in range(0, len(list), size)]


# gets the cpu count
def get_cpu_count(exclude=2):
    count = multiprocessing.cpu_count()
    return max(1, count - exclude)


# Only cuts the image using the given coordinates and extensions
def cut_image(img, bbox, pad=32):
    x, y, box_w, box_h = bbox
    shape = img.shape[0:2]
    xm = int(max(0, x - pad))
    ym = int(max(0, y - pad))
    xM = int(np.ceil(min(shape[1], x + box_w + pad)))
    yM = int(np.ceil(min(shape[0], y + box_h + pad)))
    return img[ym:yM,xm:xM,:]


# Cuts the image and the given mask using the given coordinates and extensions
def cut_image_and_mask(img, mask, bbox, pad=32):
    x, y, box_w, box_h = bbox
    shape = img.shape[0:2]
    xm = int(max(0, x - pad))
    ym = int(max(0, y - pad))
    xM = int(np.ceil(min(shape[1], x + box_w + pad)))
    yM = int(np.ceil(min(shape[0], y + box_h + pad)))
    return img[ym:yM,xm:xM,:], mask[ym:yM,xm:xM]


# converts the image to normalized float values [0,1]
def im2double(image,maxval=255.0):
    return image / maxval


# converts the image back to 256 levels per channel
def im2uint8(image):
    return (image * 255).astype(np.uint8)


# Rescales the given image and mask to fit the given size tuple (square by default)
# Credits to: https://stackoverflow.com/questions/44720580/resize-image-canvas-to-maintain-square-aspect-ratio-in-python-opencv
def resize_and_pad(img, mask, size, padColor=0):
    h, w = img.shape[:2]
    sh, sw = size
    # interpolation method
    if h > sh or w > sw:  # shrinking image
        interp = cv2.INTER_AREA
    else:  # stretching image
        interp = cv2.INTER_CUBIC
    # aspect ratio
    aspect = float(w) / h

    if aspect > 1:  # horizontal
        new_w = sw
        new_h = np.round(new_w / aspect).astype(int)
        pad_vert = (sh - new_h) / 2
        pad_top, pad_bot = np.floor(pad_vert).astype(int), np.ceil(pad_vert).astype(int)
        pad_left, pad_right = 0, 0
    elif aspect < 1:  # vertical
        new_h = sh
        new_w = np.round(new_h * aspect).astype(int)
        pad_horz = (sw - new_w) / 2
        pad_left, pad_right = np.floor(pad_horz).astype(int), np.ceil(pad_horz).astype(int)
        pad_top, pad_bot = 0, 0
    else:  # square
        new_h, new_w = sh, sw
        pad_left, pad_right, pad_top, pad_bot = 0, 0, 0, 0
    # scale and pad
    scaled_img = cv2.resize(img, (new_w, new_h), interpolation=interp)
    scaled_img = cv2.copyMakeBorder(scaled_img, pad_top, pad_bot, pad_left, pad_right, borderType=cv2.BORDER_CONSTANT, value=padColor)
    scaled_msk = cv2.resize(mask, (new_w, new_h), interpolation=interp)
    scaled_msk = cv2.copyMakeBorder(scaled_msk, pad_top, pad_bot, pad_left, pad_right, borderType=cv2.BORDER_CONSTANT, value=padColor)

    return scaled_img, scaled_msk


##########################
#         Main           #
##########################
def main():
    print("Initializing generator...")
    config = Config()
    np.random.seed(42)

    parser = argparse.ArgumentParser(description='COCO dataset utility that generates a processed set of masked images.')
    parser.add_argument('-p', '--path', help='COCO dataset directory, default: {}'.format(config.COCO_PATH), nargs='?',
                        const=config.COCO_PATH, type=str, default=config.COCO_PATH)
    parser.add_argument('-d', '--dest', help='Target directory, default: {}'.format(config.COCO_DEST), nargs='?',
                        const=config.COCO_DEST, type=str, default=config.COCO_DEST)
    parser.add_argument('-s', '--set', help='Which sets should be generated, "test" or "all" (default)', nargs='?',
                        const=config.GEN_SET, type=str, default=config.GEN_SET)
    parser.add_argument('-n', '--nohup', help='Do not ask for confirmation and execute immediately', nargs='?',
                        const=config.NOHUP, type=str, default=config.NOHUP)

    args = vars(parser.parse_args())

    # Basic setup
    COCO_PATH = args['path']
    COCO_DEST = args['dest']
    GEN_SET = args['set']
    NOHUP = args['nohup']

    # print config and ask for confirmation
    print('-'*20)
    print('{:<20s} {:<80s}'.format('COCO directory:', COCO_PATH))
    print('{:<20s} {:<80s}'.format('Target folder:', COCO_DEST))
    print('{:<20s} {:<80s}'.format('Sets to process:', GEN_SET))
    print('{:<20s} {:<80s}'.format('Target img size:', str(config.TARGET_SIZE)))
    par = 'blur: {} - dil: {} px - gauss:{} px - max area: {} px'\
        .format(config.BLUR, config.DILATION_KSIZE, config.GAUSSIAN_KSIZE, config.AREA_THRESHOLD)
    print('{:<20s} {:<80s}'.format('Parameters:', par))
    print('{:<20s} {:<80s}'.format('Test set percent.:', str(config.TEST_PERCENT)))
    print('-'*20)

    if NOHUP == 'no':
        if not query_yes_no('Continue?'):
            print('Canceled.')
            return

    if GEN_SET not in config.GEN_SET_VALS:
        raise ValueError('The specified set is not valid!')

    COCO_SUBDIRS = ['{}/{}'.format(COCO_PATH, pref) for pref in config.COCO_PREFIXES]

    # Check whether the given path exists and has the correct structure
    if not os.path.isdir(COCO_PATH):
        raise FileNotFoundError('The specified COCO path cannot be found.')
    else:
        for dir in COCO_SUBDIRS:
            if (not os.path.isdir(dir)):
                err = "{} is not a valid directory.".format(dir)
                raise ValueError(err)
        print('COCO dataset found at {}.'.format(COCO_PATH))

    # Init COCO Api with the validation set annotations, just to extract categories
    coco = cocoapi.COCO('{}/instances_{}.json'.format(COCO_SUBDIRS[-1], config.COCO_PREFIXES[1]))

    # Load categories. The ids have some missing positions, converting to a list for quicker lookup
    print('Loading categories...')
    categoriesDict = coco.loadCats(coco.getCatIds())
    categories = [None] * categoriesDict[-1]['id']
    for cat in categoriesDict:
        pos = cat['id'] - 1
        categories[pos] = cat['name']
    print('Loaded {} categories out of 80.'.format(sum(0 if x is None else 1 for x in categories)))
    frequencies = cocoutils.class_distributions()

    # Create the folder structure for Keras in the target directory
    create_dir_structure(COCO_DEST, config.KERAS_DIRS, categories)

    if GEN_SET == 'all':
        # Iterate over COCO folders (positions 0,1,2, exluding 3 since it contains annotations dir
        for i, coco_set in enumerate(COCO_SUBDIRS[:2]):
            print("Processing {}...".format(coco_set))
            coco = cocoapi.COCO('{}/instances_{}.json'.format(COCO_SUBDIRS[-1], config.COCO_PREFIXES[i]))
            dest_path = '{}/{}'.format(COCO_DEST, config.KERAS_DIRS[i])

            # load image ids
            imageIDs = coco.getImgIds()
            total = len(imageIDs)
            print("Total images in {}: {}".format(config.COCO_PREFIXES[i], total))

            #Iterate the image IDs, get the image and the annotations and produce the clipped pictures
            done = 0
            categoryCount = np.zeros(len(categories)).astype(np.int)

            # Iterate over the image IDs
            for index, id in enumerate(imageIDs):
                imgData  = coco.loadImgs([id])[0]
                image = cv2.imread("{}/{}".format(coco_set, imgData["file_name"]), cv2.IMREAD_COLOR)
                annotIDs = coco.getAnnIds(imgIds=[id])
                annotations = coco.loadAnns(annotIDs)

                j = 0 #counts the annotations for the final filename
                for ann in annotations:
                    bbox = ann['bbox']
                    catID = ann['category_id']
                    area = ann['area']
                    if bbox is None or catID is None or area is None or ann['iscrowd'] > 0:
                        continue
                    if categoryCount[catID-1] >= config.MAX_SAMPLES[i]:
                        continue
                    if frequencies[categories[catID-1]] >= config.FREQ_THRESHOLD and area <= config.AREA_THRESHOLD:
                        continue

                    #process image
                    if config.MODE == 'segment':
                        mask = coco.annToMask(ann)
                        img_cut, mask = cut_image_and_mask(im2double(image), mask, bbox)
                        img_scaled, mask = resize_and_pad(img_cut, mask, size=config.TARGET_SIZE)
                        if config.BLUR:
                            mask = cv2.dilate(mask, config.dilation_kernel, iterations=1)
                            mask = np.clip(cv2.GaussianBlur(mask.astype(np.float), config.gaussian_kernel_size, 0,
                                                            borderType=cv2.BORDER_CONSTANT), 0, 1)
                        mask = mask[...,np.newaxis]
                        result =img_scaled * mask
                        background = (1 - mask) * 0.5
                        result += background
                    else:
                        img = cut_image(im2double(image), bbox)
                        result = cv2.resize(img, config.TARGET_SIZE, interpolation=cv2.INTER_LINEAR)

                    # store result
                    name = '{}_{}.jpg'.format(ann['id'], j)
                    cat  = categories[catID-1]
                    save_image(im2uint8(result), dest_path, name, cat)

                    # update count for each category
                    categoryCount[catID-1] += 1
                    j += 1

                # update progress
                done += 1
                if index % 10 == 0:
                    progress(done, total, suffix='{}/{}'.format(done, total))

            # print result
            progress(total, total, suffix='{}/{}'.format(total, total))
            print('Completed path: {}'.format(coco_set))
            print('Generated:')
            print('-'*20)
            for k, count in enumerate(categoryCount):
                if categories[k] is None: continue
                print('{:<20s} {:<10s}'.format(categories[k], str(count)))

        # Done!
        print('Images successfully generated!')
    else:
        print('Only test set required.')

    # check paths before moving files
    print('Checking training and test sets folder structures...'.format(config.TEST_PERCENT))
    keras_tr_path = '{}/{}'.format(COCO_DEST,config.KERAS_DIRS[0]) #training folder
    keras_te_path = '{}/{}'.format(COCO_DEST,config.KERAS_DIRS[2]) #test folder

    for c in categories:
        if c is None: continue
        cdir = c.replace(' ', '_')
        p1 = '{}/{}'.format(keras_tr_path, cdir)
        p2 = '{}/{}'.format(keras_te_path, cdir)
        if not os.path.isdir(p1) or not os.path.isdir(p2):
            raise NotADirectoryError('Training and test set paths are not valid or do not exist.')

    # Paths checked, start transferring data
    print('Train and test set paths checked. Generating test set from {}% of the training data...'.format(config.TEST_PERCENT))
    catCount = len(categories)
    progress(0, catCount, suffix='{}/{} categories'.format(0, catCount))

    #iterate categories
    for i, c in enumerate(categories):
        if c is None: continue
        cdir = c.replace(' ', '_')
        src = '{}/{}'.format(keras_tr_path, cdir)
        dst = '{}/{}'.format(keras_te_path, cdir)
        files = os.listdir(src)
        count = int((len(files) * config.TEST_PERCENT) / 100)
        progress(0, catCount, suffix='{}/{} categories'.format(0, catCount))

        # Select a percentage from each category folder and move it to the corresponding test folder
        for file in np.random.choice(files, count, replace=False):
            try:
                os.rename('{}/{}'.format(src,file),'{}/{}'.format(dst,file))
            except OSError:
                continue

        #update progress
        progress(i, catCount, suffix='{}/{} categories'.format(i, catCount))

    # Done selecting data
    progress(catCount, catCount, suffix=' {}/{} categories'.format(catCount, catCount))
    print('Test set generated!')


### Execution
if __name__ == '__main__': main()



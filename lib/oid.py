# ---------------
# Date: 7/19/2018
# Place: Biella/Torino
# Author: EscVM & TArt
# Project: OID v4
# ---------------

"""
OID v4 Downloader
Download specific classes of the huge online dataset Open Image Dataset.
Licensed under the MIT License (see LICENSE for details)
------------------------------------------------------------
Usage:
"""
from sys import exit
from lib.modules.utils import *
from lib.modules.downloader import *
from lib.modules.csv_downloader import *

#ssl haxx
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

ROOT_DIR = ''
DEFAULT_OID_DIR = os.path.join(ROOT_DIR, 'OID')


def run(classes, img_options, dataset_dir='OID', csv_dir='csv_folder', t_csv='all', n_threads=None, lim=1000):
    name_file_class = 'class-descriptions-boxable.csv'
    csv_dir = os.path.join(dataset_dir, csv_dir)
    CLASSES_CSV = os.path.join(csv_dir, name_file_class)
    type_csv = "validation" if t_csv == "valid" else t_csv

    if classes is None:
        print('Missing classes argument.')
        exit(1)

    folder = ['train', 'validation', 'test']
    file_list = ['train-annotations-bbox.csv', 'validation-annotations-bbox.csv', 'test-annotations-bbox.csv']
    for f in file_list:
        error_csv(f, csv_dir, ignore=False)

    if classes[0].endswith('.txt'):
        with open(classes[0]) as f:
            classes = f.readlines()
        classes = [x.strip() for x in classes]
    else:
        classes = [arg.replace('_', ' ') for arg in classes]

        mkdirs(dataset_dir, csv_dir, classes, type_csv)

        for cls in classes:

            print("[INFO] Downloading {}.".format(cls))
            class_name = cls

            error_csv(name_file_class, csv_dir, ignore=False)
            df_classes = pd.read_csv(CLASSES_CSV, header=None)

            class_code = df_classes.loc[df_classes[1] == class_name].values[0][0]

            if type_csv == 'train':
                name_file = file_list[0]
                df_val = TTV(csv_dir, name_file)
                if n_threads is None:
                    download(img_options, type_csv, df_val, folder[0], dataset_dir, class_name, class_code, limit=lim)
                else:
                    download(img_options, type_csv, df_val, folder[0], dataset_dir, class_name, class_code,
                             threads=n_threads, limit=lim)

            elif type_csv == 'validation':
                name_file = file_list[1]
                df_val = TTV(csv_dir, name_file)
                if n_threads is not None:
                    download(img_options, type_csv, df_val, folder[1], dataset_dir, class_name, class_code, limit=lim)
                else:
                    download(img_options, type_csv, df_val, folder[1], dataset_dir, class_name, class_code,
                             threads=n_threads, limit=lim)

            elif type_csv == 'test':
                name_file = file_list[2]
                df_val = TTV(csv_dir, name_file)
                if n_threads is not None:
                    download(img_options, type_csv, df_val, folder[1], dataset_dir, class_name, class_code, limit=lim)
                else:
                    download(img_options, type_csv, df_val, folder[1], dataset_dir, class_name, class_code,
                             threads=n_threads, limit=lim)

            elif type_csv == 'all':
                for i in range(3):
                    name_file = file_list[i]
                    df_val = TTV(csv_dir, name_file)
                    if n_threads is not None:
                        download(img_options, type_csv, df_val, folder[1], dataset_dir, class_name, class_code, limit=lim)
                    else:
                        download(img_options, type_csv, df_val, folder[1], dataset_dir, class_name, class_code,
                                 threads=n_threads, limit=lim)
            else:
                print('[ERROR] csv file not specified')
                exit(1)

def oid_to_coco_categories():
    return ['Person', 'Bicycle', 'Car', 'Motorcycle', 'Airplane',
            'Bus', 'Train', 'Truck', 'Boat', 'Traffic light',
            'Fire hydrant', 'Stop sign', 'Parking meter', 'Bench', 'Bird',
            'Cat', 'Dog', 'Horse', 'Sheep', 'Cattle', 'Elephant', 'Bear',
            'Zebra', 'Giraffe', 'Backpack', 'Umbrella', 'Handbag', 'Tie',
            'Suitcase', 'Flying disc', 'Ski', 'Snowboard', 'Ball',
            'Kite', 'Baseball bat', 'Baseball glove', 'Skateboard',
            'Surfboard', 'Tennis racket', 'Bottle', 'Wine glass', 'Mug',
            'Fork', 'Knife', 'Spoon', 'Bowl', 'Banana', 'Apple',
            'Sandwich', 'Orange', 'Broccoli', 'Carrot', 'Hot dog', 'Pizza',
            'Doughnut', 'Cake', 'Chair', 'Couch', 'Houseplant', 'Bed',
            'Kitchen & dining room table', 'Toilet', 'Television', 'Laptop', 'Computer mouse', 'Remote control',
            'Computer keyboard', 'Mobile phone', 'Microwave oven', 'Oven', 'Toaster',
            'Sink', 'Refrigerator', 'Book', 'Clock', 'Vase', 'Scissors',
            'Teddy bear', 'Hair dryer', 'Toothbrush']

def oid_to_coco_catdirs():
    dirs = [c.replace(" ", "_") for c in oid_to_coco_categories()]
    return dirs
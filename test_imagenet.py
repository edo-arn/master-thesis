from __future__ import print_function

from lib import inception
from keras.optimizers import Adam
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.inception_v3 import preprocess_input
import json
import numpy as np
import pandas as pd
import argparse
import os
from numpy import genfromtxt

try:
    from keras.engine import saving
except ImportError: # Keras before 2.2 used the 'topology' namespace.
    from keras.engine import topology as saving

# Uncomment this for cpu-only usage
#import os
#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"


# Config data
######################

class TestConfig(object):
    DATA_PATH = '/data/imagenet/imagenet-data/raw-data/train'
    WEIGHTS = 'imagenet'
    
    TARGET_SIZE = (299, 299)
    BATCH_SIZE = 64
    SYNSETS_PATH = os.path.join('labels', 'imagenet-id-synsets.json')
    LABELS_PATH = os.path.join('labels', 'coco-imagenet-10-synsets.json')

    RESULT_PATH = './tests'
    RESULT_NAME = 'imagenet-predictions.csv'
    ALLOWED_ACTIONS = ['test', 'predict', 'extract']
    ACTION = 'extract'


def json_dictionary(path):
    with open(path) as file:
        raw_data = json.load(file)
    return {int(k): v for k, v in raw_data.items()}


# Main
######################
def main():
    # create config
    cfg = TestConfig()

    # init argparse
    parser = argparse.ArgumentParser(description='COCO dataset utility that generates a processed set of masked images.')
    parser.add_argument('-s', '--set', help='test set directory', nargs='?', const=cfg.DATA_PATH, type=str,
                        default=cfg.DATA_PATH)
    parser.add_argument('-a', '--action', help='what to do: test, predict or extract.', nargs='?', const=cfg.ACTION,
                        type=str, default=cfg.ACTION)
    parser.add_argument('-r', '--result', help='result name, if anything gets saved.', nargs='?', const=cfg.RESULT_NAME,
                        type=str, default=cfg.RESULT_NAME)

    args = parser.parse_args()
    if args.action not in cfg.ALLOWED_ACTIONS:
        raise ValueError("Action '{}' not recognised. Allowed actions: {}".format(args.action, cfg.ALLOWED_ACTIONS))

    print('-'*20)
    print('{:<20s} {:<80s}'.format('Data path:', args.set))
    print('{:<20s} {:<80s}'.format('Action:', args.action))
    print('{:<20s} {:<80s}'.format('Result:', args.result))
    print('-'*20)

    #load labels
    with open(cfg.SYNSETS_PATH) as f:
        imagenet_synsets = json.load(f)
    id_to_synset = {int(k): v['id'] for k, v in imagenet_synsets.items()}
    labels_dict = json_dictionary(cfg.LABELS_PATH)
    class_names = labels_dict.values()
    synset_to_label = {'{}-n'.format(name[1:]): int(index) for index, name in labels_dict.items()}

    print('building model...', end=' ')
    extract = args.action == 'extract'
    model = inception.build_model(class_names, weights=cfg.WEIGHTS, extract_rep=extract, top=True)
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    print('done')

    #prepare generator
    generator = ImageDataGenerator(preprocessing_function=preprocess_input)
    test_batches = generator.flow_from_directory(args.set,
                                                 batch_size=cfg.BATCH_SIZE,
                                                 classes=class_names,
                                                 target_size=cfg.TARGET_SIZE,
                                                 shuffle=False)

    # test model
    filenames = test_batches.filenames
    steps = int(np.ceil(len(filenames)/cfg.BATCH_SIZE))

    if args.action == 'predict':
        print('predicting categories...', end=' ')
        result = model.predict_generator(test_batches, steps=steps, verbose=1)
        print('done')
        # save model
        print('saving results to {} ...'.format(cfg.RESULT_PATH), end=' ')
        pd.DataFrame(test_batches.classes).to_csv('{}/{}'.format(cfg.RESULT_PATH, 'true_{}'.format(args.result)))
        pd.DataFrame(result).to_csv('{}/{}'.format(cfg.RESULT_PATH, args.result))
        print('done')

    elif args.action == 'test':
        print('testing set...')
        print(model.output, model.output_shape)
        result = model.predict_generator(test_batches, steps=steps, verbose=1)
        true_labels = test_batches.classes
        predictions = np.argmax(result, axis=-1)
        synsets = [id_to_synset[p] for p in predictions]
        predictions = np.array([synset_to_label.get(s, -1) for s in synsets])
        correct = (true_labels == predictions).sum()
        print('acc: {:.5f}'.format(float(correct) / len(predictions)))

    elif args.action == 'extract':
        print('predicting categories and extracting representations...')
        representations, predictions = model.predict_generator(test_batches, steps=steps, verbose=1)
        print("Rep. shape: ", representations.shape)
        print("Pred. shape:",predictions.shape)

        true_labels = test_batches.classes
        predictions = np.argmax(predictions, axis=-1)
        synsets = [id_to_synset[p] for p in predictions]
        predictions = np.array([synset_to_label.get(s, -1) for s in synsets])
        indices = np.where(true_labels == predictions)[0]

        print(indices.shape)
        print("{} predictions out of {}".format(indices.size, true_labels.size))
        assert (true_labels[indices] == predictions[indices]).all(), 'Selected indices are not correct!'

        data = np.empty((indices.size, representations.shape[1] + 1))
        print("Saving representations and labels into a csv file [shape {}]...".format(data.shape), end="")
        data[:,:-1] = representations[indices]
        data[:,-1]  = true_labels[indices]
        np.save("{}/{}".format(cfg.RESULT_PATH, args.result), data)
        print("done.")

    elif args.action =='extract-parallel':
        print('Action not supported yet.')
        exit(1)

        print('Extracting representations from bbox and segmentation sets...')
        print('Reading true labels and predictions...', end='')
        #gen from txt keeps the row index and adds a row for column names
        y_true = genfromtxt(os.path.join(cfg.RESULT_PATH, 'true.csv'), delimiter=',')[1:,1:].flatten()
        y_pred_b = genfromtxt(os.path.join(cfg.RESULT_PATH, 'pred_bbox.csv'), delimiter=',')[1:,1:]
        y_pred_s = genfromtxt(os.path.join(cfg.RESULT_PATH, 'pred_segm.csv'), delimiter=',')[1:,1:]
        print('done.')

        assert y_pred_b.shape == y_pred_s.shape, 'Datasets do not match!'
        assert y_true.size == y_pred_b.shape[0], 'Not the same amount of data!'

        print('segm.: {}, bbox: {}, true: {}'.format(y_pred_s.shape, y_pred_b.shape, y_true.shape))
        y_labels_s = np.argmax(y_pred_s, axis=-1)
        y_labels_b = np.argmax(y_pred_b, axis=-1)

        labels = np.vstack((y_true, y_labels_s, y_labels_b)).transpose()
        equals = np.array([(row == row[0]).all() for row in labels])
        print('labels matrix: {}'.format(labels.shape))

        print('Available {}/{} representations'.format(np.count_nonzero(equals), len(equals)))
        available_indices = equals.nonzero()
        print('Saving indices to file...', end='')
        np.save('tests/indices.npy',available_indices)
        print('done.')
    print('testing done.')

### Execution
if __name__ == '__main__': main()

from __future__ import print_function

from lib import cocoutils, inception
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import TensorBoard, ModelCheckpoint
from keras.applications.inception_v3 import preprocess_input
import numpy as np
import pandas as pd
import h5py
import time
import argparse
import os
import ssl
ssl._create_default_https_context = ssl._create_unverified_context

try:
    from keras.engine import saving
except ImportError:  # Keras before 2.2 used the 'topology' namespace.
    from keras.engine import topology as saving


# Config data
######################

class TrainConfig(object):
    MODEL_NAME = 'inception'
    MODEL_PATH  = '' #'/usr/home/studenti/sp160362/project/models/inception/inception_coco_2.h5'
    RESULT_PATH = '../models/inception'
    RESULT_NAME = 'inception_bbox[toponly]_[epochs]ep.h5'

    PATH_TRAIN = '/usr/home/studenti/sp160362/data/bbox/train'
    PATH_VALID = '/usr/home/studenti/sp160362/data/bbox/valid'
    PATH_TEST  = '/usr/home/studenti/sp160362/data/bbox/test'

    LOG_DIR = './logs'
    CHECKPOINT_DIR = '../models/checkpoints'

    CLASS_NAMES = cocoutils.categories()[1:]
    CLASS_DIRS  = cocoutils.catdirs()[1:]
    TARGET_SIZE = (299, 299)
    MEAN_PIXEL = [0.5, 0.5, 0.5]
    BATCH_SIZE = 64
    NUM_EPOCHS = 20
    LEARNING_RATE = 1e-2 #prev 1e-3
    TOP_ONLY = False

    def show(self):
        print('-' * 20)
        print('{:<20s} {:<80s}'.format('COCO Inception path:', self.MODEL_PATH))
        print('{:<20s} {:<80s}'.format('Target folder:', self.RESULT_PATH))
        print('{:<20s} {:<80s}'.format('Image size:', str(self.TARGET_SIZE)))
        print('{:<20s} {:<80s}'.format('Batch size:', str(self.BATCH_SIZE)))
        print('{:<20s} {:<80s}'.format('Epochs:', str(self.NUM_EPOCHS)))
        print('{:<20s} {:<80s}'.format('L. Rate:', str(self.LEARNING_RATE)))
        print('-' * 20)


# Load weights taken from previous model
######################
def load_weights(model, path, exclude):
    print('loading weights...', end=' ')
    file = h5py.File(path, mode='r')
    f = file
    if 'layer_names' not in f.attrs and 'model_weights' in file:
        f = f['model_weights']
    layers = model.layers
    layers = filter(lambda l: l.name not in exclude, layers)
    saving.load_weights_from_hdf5_group_by_name(f, layers)
    print('done')
    file.close()


# Gets an id for the current training session
# Returns the last one if iter > 0, otherwise it generates a new one
######################
def get_train_id(history, iteration):
    last = history.tail(1)
    curr_id = last['id'].values[0]
    return curr_id if iteration > 0 else curr_id + 1


# Gets the epochs from history
######################
def get_prev_epochs(history, trainID):
    summed = history.groupby(['id']).sum()
    return int(summed.values[trainID][1]) #column id for epochs


# Gets the most recent filename from the log dir
######################
def get_previous_log(log_dir):
    prev = ""
    best_time = -1
    dirs = [ n for n in os.listdir(log_dir) if os.path.isdir(os.path.join(log_dir, n)) ]
    for dir in dirs:
        time = int(dir.split("-")[-1])
        if (time > best_time):
            prev = dir
            best_time = time
    return prev


def save_history(history, trainID, iter, epochs, result_name, optimizer, cfg):
    row = [trainID, iter, cfg.MODEL_NAME, cfg.PATH_TRAIN[:-6], epochs,
           optimizer, cfg.LEARNING_RATE, cfg.BATCH_SIZE, cfg.TOP_ONLY, result_name]
    history.loc[history.shape[0]] = row
    with open("{}/history.log".format(cfg.LOG_DIR), 'a') as file:
        history.tail(1).to_csv(file, header=False, index=False)


# Generates the filename for the model weights
######################
def format_name(template, cfg, epochs):
    name = template.replace("[epochs]", "{}".format(epochs))
    name = name.replace("[toponly]", "{}".format("_top-only" if cfg.TOP_ONLY else ""))
    return name


# Main
######################
def main():
    # create config
    cfg = TrainConfig()
    cfg.show()
    history = pd.read_csv("{}/history.log".format(cfg.LOG_DIR))

    # limit memory allocation
    #tf_config = tf.ConfigProto()
    #tf_config.gpu_options.per_process_gpu_memory_fraction = 0.8
    #set_session(tf.Session(config=tf_config))

    # init argparse
    parser = argparse.ArgumentParser(
        description='COCO dataset utility that generates a processed set of masked images.')
    parser.add_argument('-w', '--weights', help='model weights file path (default is empty)', nargs='?',
                        const=cfg.MODEL_PATH, type=str, default=cfg.MODEL_PATH)
    parser.add_argument('-n', '--name', help='name for the resulting h5 file', nargs='?',
                        const=cfg.RESULT_NAME, type=str, default=cfg.RESULT_NAME)
    parser.add_argument('-l', '--lr', help='learning rate for the training', type=float, default=cfg.LEARNING_RATE)
    parser.add_argument('-e', '--epochs', help='number of epochs (default 10)', nargs='?',
                        const=cfg.NUM_EPOCHS, type=int, default=cfg.NUM_EPOCHS)
    parser.add_argument('-i', '--iter', help='training iteration', nargs='?', const=0, type=int, default=0)

    args = vars(parser.parse_args())
    RESULT_NAME = args['name']
    NUM_EPOCHS = args['epochs']
    WEIGHTS = args['weights']
    ITER = args['iter']

    trainID = get_train_id(history, ITER)
    print('\nTRAINING SESSION #{} (iteration {})'.format(trainID, ITER))
    print('-'*20)

    print('building model and loading weights...', end=' ')
    WEIGHTS = WEIGHTS if WEIGHTS != "" else None
    model = inception.build_model(cfg.CLASS_NAMES, WEIGHTS, train_core=(not cfg.TOP_ONLY))
    print('done')

    # compile model
    opt = optimizers.Adam(lr=args['lr'])
    opt_name = type(opt).__name__
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
    print('done')
    model.summary()

    # prepare generator
    proc_function = inception.custom_process_input
    train_generator = ImageDataGenerator(preprocessing_function=proc_function,
                                         width_shift_range=0.2,
                                         height_shift_range=0.2,
                                         shear_range=0.2,
                                         zoom_range=0.2,
                                         horizontal_flip=True)
    val_generator = ImageDataGenerator(preprocessing_function=proc_function)
    train_generator.mean = np.array(cfg.MEAN_PIXEL, dtype=np.float32).reshape((1, 1, 3))
    val_generator.mean = np.array(cfg.MEAN_PIXEL, dtype=np.float32).reshape((1, 1, 3))

    train_batches = train_generator.flow_from_directory(cfg.PATH_TRAIN,
                                                        batch_size=cfg.BATCH_SIZE,
                                                        classes=cfg.CLASS_DIRS,
                                                        target_size=cfg.TARGET_SIZE,
                                                        shuffle=True)
    valid_batches = val_generator.flow_from_directory(cfg.PATH_VALID,
                                                      batch_size=cfg.BATCH_SIZE,
                                                      classes=cfg.CLASS_DIRS,
                                                      target_size=cfg.TARGET_SIZE,
                                                      shuffle=False)

    # calculate steps required for a full iteration over the set
    #tsteps = int(np.ceil(len(train_batches.classes) / float(cfg.BATCH_SIZE)))
    #vsteps = int(np.ceil(len(valid_batches.classes) / float(cfg.BATCH_SIZE)))
    tsteps = 1000
    vsteps = 10

    # calculate weights to cope with imbalanced classes
    c_w = cocoutils.class_weights_generator(train_batches, smoothing=0.15)

    # Callbacks
    log_name = get_previous_log(cfg.LOG_DIR) if ITER > 0 else ''
    log_name = 'inception-bbox-{}'.format(int(time.time())) if log_name == '' else log_name
    log_part = log_name.split('-')[:2]
    chk_name = '{}/{}-{}.*replace*'.format(cfg.CHECKPOINT_DIR, log_part[0], log_part[1]) \
        .replace('*replace*', 'weights.improvement.{epoch:02d}-{val_loss:.2f}.hdf5')

    prev_ep = get_prev_epochs(history, trainID) if ITER > 0 else 0
    tboard = TensorBoard(log_dir='{}/{}'.format(cfg.LOG_DIR, log_name))
    #red_lr = ReduceLROnPlateau(monitor='val_loss', mode='auto', factor=0.5, patience=4, min_lr=cfg.LEARNING_RATE*0.1, epsilon=0.001)
    checkp = ModelCheckpoint(chk_name, monitor='val_acc', mode='max', verbose=1, save_best_only=True,
                             save_weights_only=True)


    #Save history and fit
    model_name = format_name(RESULT_NAME, cfg, prev_ep + NUM_EPOCHS)
    save_history(history, trainID, ITER, NUM_EPOCHS, model_name, opt_name, cfg)
    model.fit_generator(train_batches,
                        steps_per_epoch=tsteps,
                        validation_data=valid_batches,
                        validation_steps=vsteps,
                        epochs=prev_ep + NUM_EPOCHS,
                        class_weight=c_w,
                        verbose=1,
                        initial_epoch=prev_ep,
                        callbacks=[tboard, checkp])

    # save model
    print('saving model weights to {}...'.format(cfg.RESULT_PATH), end=' ')
    model.save_weights("{}/{}".format(cfg.RESULT_PATH, model_name))
    print('done')


### Execution
if __name__ == '__main__': main()
